import { Theme } from '@react-navigation/native'
import React from 'react'
import { Image, Pressable, StyleSheet, Text, View } from 'react-native'
import { User } from '../models'

const getStyles = (theme: Theme) => {
  const { colors } = theme
  return StyleSheet.create({
    card: {
      height: 100,
      borderBottomColor: colors.border,
      borderBottomWidth: 2,
      marginVertical: 5,
      marginHorizontal: 10,
      flexDirection: 'row',
      alignItems: 'center'
    },
    imageContainer: {
      flex: 0.3
    },
    image: {
      height: 70,
      width: 70,
      borderRadius: 35
    },
    detailsContainer: {
      flex: 0.7
    },
    email: {
      color: colors.text,
      fontSize: 14
    }
  })
}

const Card = (args: { theme: Theme; item: User; onPress: (item: User) => void }) => {
  const { item, theme, onPress } = args
  const styles = getStyles(theme)
  return (
    <Pressable style={styles.card} onPress={() => onPress(item)}>
      <View style={styles.imageContainer}>
        <Image style={styles.image} source={{ uri: item.avatar }} />
      </View>
      <View style={styles.detailsContainer}>
        <Text style={styles.email}>
          {item.first_name} {item.last_name}
        </Text>
        <Text style={styles.email}>{item.email}</Text>
      </View>
    </Pressable>
  )
}

export const UserCard = React.memo(Card)
