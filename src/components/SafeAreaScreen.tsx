import React, { ReactChild } from 'react'
import { View } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context'

interface Props {
  children: ReactChild
}

export const SafeAreaScreen = ({ children }: Props) => {
  const insets = useSafeAreaInsets()
  return (
    <View
      style={{
        paddingTop: insets.top,
        paddingBottom: insets.bottom
      }}>
      {children}
    </View>
  )
}
