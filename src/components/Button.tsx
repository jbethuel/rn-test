import { Theme } from '@react-navigation/native'
import React from 'react'
import { Pressable, StyleSheet, Text } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const getStyles = (theme: Theme) => {
  const { colors } = theme
  return StyleSheet.create({
    button: {
      marginHorizontal: 20,
      marginVertical: 8,
      borderRadius: 15,
      borderWidth: 1,
      height: 40,
      justifyContent: 'center',
      alignItems: 'center'
    },
    label: {
      color: colors.text,
      fontWeight: 'bold'
    },
    primary: {
      backgroundColor: colors.primary
    },
    danger: {
      backgroundColor: 'red'
    }
  })
}

export const Button = (args: { type: 'primary' | 'danger'; theme: Theme; label: string; onPress: () => void }) => {
  const styles = getStyles(args.theme)
  const buttonStyle = args.type === 'primary' ? styles.primary : styles.danger
  return (
    <TouchableOpacity style={[styles.button, buttonStyle]} onPress={args.onPress}>
      <Text style={styles.label}>{args.label}</Text>
    </TouchableOpacity>
  )
}
