import { Theme } from '@react-navigation/native'
import React from 'react'
import { Keyboard, StyleSheet, TextInput as Input } from 'react-native'

const getStyles = (theme: Theme) => {
  const { colors } = theme
  return StyleSheet.create({
    container: {
      height: 50,
      borderBottomWidth: 1,
      borderColor: colors.border,
      marginHorizontal: 20,
      color: colors.text
    }
  })
}

const Form = (args: { theme: Theme; placeholder: string; value: string; onChange: (value: string) => void }) => {
  const { placeholder, value, onChange } = args
  const styles = getStyles(args.theme)
  return (
    <Input
      style={styles.container}
      placeholder={placeholder}
      defaultValue={value}
      value={value}
      onChangeText={onChange}
      onSubmitEditing={() => Keyboard.dismiss()}
      autoCapitalize="none"
      autoCorrect={false}
    />
  )
}

export const TextInput = React.memo(Form)
