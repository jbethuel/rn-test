import React from 'react'
import { View, Text } from 'react-native'
import { render } from '@testing-library/react-native'

const Sample = () => {
  return (
    <View>
      <Text testID="sample">Sample!</Text>
    </View>
  )
}

it('renders correctly', () => {
  const { getByTestId } = render(<Sample />)
  const text = getByTestId('sample')

  expect(text.children[0]).toBe('Sample!')
})
