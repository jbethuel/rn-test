import { Theme, useTheme } from '@react-navigation/native'
import React, { useEffect, useState } from 'react'
import { ActivityIndicator, Alert, StyleSheet, View } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import { UserCard } from '../components'
import { User } from '../models'
import { users } from '../utils/api'
import { HomeScreenNavigationProp } from '../utils/navigation'

const getStyles = (theme: Theme) => {
  const { colors } = theme
  return StyleSheet.create({
    loadingContainer: { flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: colors.background },
    fill: { flex: 1, backgroundColor: colors.background }
  })
}

export const HomeScreen = ({ navigation, route }: HomeScreenNavigationProp) => {
  const theme = useTheme()
  const styles = getStyles(theme)
  const [usersList, setUsers] = useState<User[]>([])

  const catchError = () => {
    Alert.alert('Error', 'Request failed', [
      { onPress: () => fetchUsers(), text: 'retry' },
      { onPress: () => {}, text: 'ok' }
    ])
  }

  const fetchUsers = async () => {
    try {
      setUsers(await users(1))
    } catch (e) {
      catchError()
    }
  }

  useEffect(() => {
    fetchUsers()
  }, [])

  useEffect(() => {
    if (!route.params?.user) return

    switch (route.params.type) {
      case 'update': {
        const usersCopy = usersList.filter(user => user.id !== route.params.user?.id)
        setUsers([...usersCopy, route.params.user].reverse())
        break
      }
      case 'delete': {
        const usersCopy = usersList.filter(user => user.id !== route.params.user?.id)
        setUsers(usersCopy)
        if (!usersCopy.length) fetchUsers()
        break
      }
      default: {
        break
      }
    }
  }, [route.params?.type, route.params?.user])

  if (!usersList.length)
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" />
      </View>
    )

  return (
    <View style={styles.fill}>
      <FlatList
        data={usersList}
        keyExtractor={({ id }) => id.toString()}
        renderItem={({ item }) => (
          <UserCard item={item} theme={theme} onPress={user => navigation.navigate('Profile', { user })} />
        )}
      />
    </View>
  )
}
