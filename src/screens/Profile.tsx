import { Theme, useTheme } from '@react-navigation/native'
import React, { useState } from 'react'
import { Image, StyleSheet } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { Button, TextInput } from '../components'
import { ProfileScreenNavigationProp } from '../utils'

const getStyles = (theme: Theme) => {
  const { colors } = theme
  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.background
    },
    avatar: {
      height: 100,
      width: 100,
      borderRadius: 50,
      alignSelf: 'center',
      margin: 20
    }
  })
}

export const Profile = ({ navigation, route }: ProfileScreenNavigationProp) => {
  const theme = useTheme()
  const styles = getStyles(theme)
  const { user } = route.params

  const [userInfo, setUserInfo] = useState(user)

  return (
    <ScrollView style={styles.container}>
      <Image style={styles.avatar} source={{ uri: user.avatar }} />
      <TextInput
        theme={theme}
        placeholder="Email"
        value={userInfo.email}
        onChange={value => setUserInfo({ ...userInfo, email: value })}
      />
      <TextInput
        theme={theme}
        placeholder="First Name"
        value={userInfo.first_name}
        onChange={value => setUserInfo({ ...userInfo, first_name: value })}
      />
      <TextInput
        theme={theme}
        placeholder="Last Name"
        value={userInfo.last_name}
        onChange={value => setUserInfo({ ...userInfo, last_name: value })}
      />
      <Button
        type="primary"
        label="Update Details"
        theme={theme}
        onPress={() => navigation.navigate('Home', { type: 'update', user: userInfo })}
      />
      <Button
        type="danger"
        label="Delete User"
        theme={theme}
        onPress={() => navigation.navigate('Home', { type: 'delete', user: userInfo })}
      />
    </ScrollView>
  )
}
