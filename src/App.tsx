import { DarkTheme, DefaultTheme, NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import { useColorScheme } from 'react-native'
import { HomeScreen } from './screens/Home'
import { Profile } from './screens/Profile'
import { RootStackParamList } from './utils/navigation'

// you can now set initialParams in RootStackNavigator.Screen according to RootStackParamList
const RootStackNavigator = createStackNavigator<RootStackParamList>()

function App() {
  const scheme = useColorScheme()
  return (
    <NavigationContainer theme={scheme === 'dark' ? DarkTheme : DefaultTheme}>
      <RootStackNavigator.Navigator>
        <RootStackNavigator.Screen name="Home" component={HomeScreen} />
        <RootStackNavigator.Screen name="Profile" component={Profile} />
      </RootStackNavigator.Navigator>
    </NavigationContainer>
  )
}

export default App
