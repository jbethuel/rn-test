import { StackScreenProps } from '@react-navigation/stack'
import { User } from '../models'

export type RootStackParamList = {
  Home: {
    type?: 'delete' | 'update'
    user?: User
  }
  Profile: { user: User }
}

export type HomeScreenNavigationProp = StackScreenProps<RootStackParamList, 'Home'>
export type ProfileScreenNavigationProp = StackScreenProps<RootStackParamList, 'Profile'>
