import Reactotron from 'reactotron-react-native'

export const reactotron = Reactotron.configure().useReactNative().connect()
export const log = (args: unknown) => reactotron.log!(args)
export const warn = (args: unknown) => reactotron.warn!(args)