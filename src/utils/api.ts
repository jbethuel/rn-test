import axios from 'axios'
import { User } from '../models'

const instance = axios.create({
  baseURL: 'https://reqres.in/api',
  timeout: 1000,
  headers: {
    Accept: 'application/json',
    'Content-type': 'application/json'
  }
})

axios.interceptors.response.use(
  function (response) {
    return response.data.data
  },
  function (error) {
    return Promise.reject(error)
  }
)

export const users = async (page: number) => {
  const response = await instance.get(`/users?page=${page}`)
  return response.data.data as User[]
}
